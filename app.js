let currentActivity;
const btnGetActivity = document.getElementById("btn-get-activity");
const hActivityText = document.getElementById("h3-act-text");

btnGetActivity.addEventListener("click", getNewActivity);

async function getNewActivity() {
  let response = await fetch("https://www.boredapi.com/api/activity");
  let result = await response.json();
  currentActivity=result
  updateActivity()
}

function updateActivity() {
  hActivityText.innerText = currentActivity.activity;
}
